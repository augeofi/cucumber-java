#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: macu.feature

Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	413149|	/xfer/TEST/automation/macu/busin/client/bin1.txt|/ncrp/MACU/BUSIN/WD/macu.busin.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookAppBin1.bash" from app1
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookDSBin1.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Jon	|
	  | lastName			|	Paulson	|
	  | address1			|	1720 SUPERBIN1 ROAD|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|1721 SUPERBIN1 ROAD|
	  | address2			|	APT 1	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6504152102	|
		| firstName  | Ramesh |
		| lastName			|	Alreja	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	C	|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6504152103	|
		| lastName			|	Bertrand	|
		| firstName     | Yogesh    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6504152104	|
		| lastName			|	Ross	|
		| firstName     | Barney    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	E		|
		| status			|	C|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6504152105	|
		| lastName			|	Chapman	|
		| firstName     | Charles  |
	#	| ptsExpirePeriod			|	4	|
	And verified on getAccountApi
		| unique user		|	F		|
	  | firstName			|	Joan	|
	  | lastName			|	Summers	|
	  | address1			|	1725 SUPERBIN1 ROAD|
	  | address2			|	APT 6	|
		| status			|	C	|
	And verified on getAccountApi
		| unique user		|	G		|
	  | firstName			|	Charles	|
	  | lastName			|	Simon	|
	  | address1			|	1726 SUPERBIN1 ROAD|
	  | address2			|	APT 7	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	H		|
	  | firstName			|	Jack	|
	  | lastName			|	Taylor	|
	  | address1			|	1727 SUPERBIN1 ROAD|
	  | address2			|	APT 8	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	I		|
	  | firstName			|	Fred	|
	  | lastName			|	Costello	|
	  | address1			|	1728 SUPERBIN1 ROAD|
	  | address2			|	APT 9	|
		| status			 |	X	|
		And verified on getAccountApi
		| unique user		|	J		|
	  | firstName			|	Nick	|
	  | lastName			|	Jagger	|
	  | address1			|	1729 SUPERBIN1 ROAD|
	  | address2			|	APT 10	|
		| status			  |	X	|
	
	

	And I dispose of the ssh
	
	Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu for bin 489662
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	489662|	/xfer/TEST/automation/macu/busin/client/bin2.txt|/ncrp/MACU/BUSIN/WD/macu.busin.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookAppBin2.bash" from app1
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookDSBin2.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Jack	|
	  | lastName			|	Jolson	|
	  | address1			|	1820 SUPERBIN2 ROAD|
	  | address2			|	APT 1	|
   | city           |       WHEATON |
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|1821 SUPERBIN2 ROAD|
	  | address2			|	APT 1	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| firstName  | Satish |
		| lastName			|	Raheja	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	C	|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152103	|
		| lastName			|	Gursahani	|
		| firstName     | Marsha    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Sanders	|
		| firstName     | Craig    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	E		|
		| status			|	C|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152105	|
		| lastName			|	Chhabria	|
		| firstName     | Amaya  |
	#	| ptsExpirePeriod			|	4	|
	And verified on getAccountApi
		| unique user		|	F		|
	  | firstName			|	Matthew	|
	  | lastName			|	Finch	|
	  | address1			|	1825 SUPERBIN2 ROAD|
	  | address2			|	APT 6	|
		| status			|	C	|
	And verified on getAccountApi
		| unique user		|	G		|
	  | firstName			|	Jack	|
	  | lastName			|	Kellogg	|
	  | address1			|	1826 SUPERBIN2 ROAD|
	  | address2			|	APT 7	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	H		|
	  | firstName			|	Albert	|
	  | lastName			|	Kennedy |
	  | address1			|	1827 SUPERBIN2 ROAD|
	  | address2			|	APT 8	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	I		|
	  | firstName			|	Joel	|
	  | lastName			|	Lambert	|
	  | address1			|	1828 SUPERBIN2 ROAD|
	  | address2			|	APT 9	|
		| status			 |	X	|
		And verified on getAccountApi
		| unique user		|	J		|
	  | firstName			|	Joe	|
	  | lastName			|	Wong	|
	  | address1			|	1829 SUPERBIN2 ROAD|
	  | address2			|	APT 10	|
		| status			  |	X	|
	
	

	And I dispose of the ssh
	
	Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu for bin 412248
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	412248|	/xfer/TEST/automation/macu/busin/client/bin3.txt|/ncrp/MACU/BUSIN/WD/macu.busin.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookAppBin3.bash" from app1
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookDSBin3.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Jack	|
	  | lastName			|	Jolson	|
	  | address1			|	1820 SUPERBIN3 ROAD|
	  | address2			|	APT 1	|
   | city           |       WHEATON |
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|1821 SUPERBIN3 ROAD|
	  | address2			|	APT 1	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| firstName  | Satish |
		| lastName			|	Raheja	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	C	|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152103	|
		| lastName			|	Gursahani	|
		| firstName     | Marsha    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Sanders	|
		| firstName     | Craig    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	E		|
		| status			|	C|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152105	|
		| lastName			|	Chhabria	|
		| firstName     | Amaya  |
	#	| ptsExpirePeriod			|	4	|
	And verified on getAccountApi
		| unique user		|	F		|
	  | firstName			|	Matthew	|
	  | lastName			|	Finch	|
	  | address1			|	1825 SUPERBIN3 ROAD|
	  | address2			|	APT 6	|
		| status			|	C	|
	And verified on getAccountApi
		| unique user		|	G		|
	  | firstName			|	Jack	|
	  | lastName			|	Kellogg	|
	  | address1			|	1826 SUPERBIN3 ROAD|
	  | address2			|	APT 7	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	H		|
	  | firstName			|	Albert	|
	  | lastName			|	Kennedy |
	  | address1			|	1827 SUPERBIN3 ROAD|
	  | address2			|	APT 8	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	I		|
	  | firstName			|	Joel	|
	  | lastName			|	Lambert	|
	  | address1			|	1828 SUPERBIN3 ROAD|
	  | address2			|	APT 9	|
		| status			 |	X	|
		And verified on getAccountApi
		| unique user		|	J		|
	  | firstName			|	Joe	|
	  | lastName			|	Wong	|
	  | address1			|	1829 SUPERBIN3 ROAD|
	  | address2			|	APT 10	|
		| status			  |	X	|
	
	

	And I dispose of the ssh
	
	Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu for bin 412248
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	439857|	/xfer/TEST/automation/macu/busin/client/bin4.txt|/ncrp/MACU/BUSIN/WD/macu.busin.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookAppBin4.bash" from app1
    When I run runbook "/xfer/TEST/automation/macu/busin/client/runbookDSBin4.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Jill	|
	  | lastName			|	Jolson	|
	  | address1			|	1820 SUPERBIN4 ROAD|
	  | address2			|	APT 1	|
   | city           |       WHEATON |
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|1821 SUPERBIN4 ROAD|
	  | address2			|	APT 1	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| firstName  | Santosh |
		| lastName			|	Raheja	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	C	|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152103	|
		| lastName			|	Albertson	|
		| firstName     | Tamara   |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Sanders	|
		| firstName     | Craig    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	E		|
		| status			|	C|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152105	|
		| lastName			|	Chhabria	|
		| firstName     | Amaya  |
	#	| ptsExpirePeriod			|	4	|
	And verified on getAccountApi
		| unique user		|	F		|
	  | firstName			|	Matthew	|
	  | lastName			|	Finch	|
	  | address1			|	1825 SUPERBIN4 ROAD|
	  | address2			|	APT 6	|
		| status			|	C	|
	And verified on getAccountApi
		| unique user		|	G		|
	  | firstName			|	Jill	|
	  | lastName			|	Kellogg	|
	  | address1			|	1826 SUPERBIN4 ROAD|
	  | address2			|	APT 7	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	H		|
	  | firstName			|	Albert	|
	  | lastName			|	Kennedy |
	  | address1			|	1827 SUPERBIN4 ROAD|
	  | address2			|	APT 8	|
		| status			 |	L	|
		And verified on getAccountApi
		| unique user		|	I		|
	  | firstName			|	Joel	|
	  | lastName			|	Lambert	|
	  | address1			|	1828 SUPERBIN4 ROAD|
	  | address2			|	APT 9	|
		| status			 |	X	|
		And verified on getAccountApi
		| unique user		|	J		|
	  | firstName			|	Joe	|
	  | lastName			|	Wong	|
	  | address1			|	1829 SUPERBIN4 ROAD|
	  | address2			|	APT 10	|
		| status			  |	X	|
	
	

	And I dispose of the ssh
	
	