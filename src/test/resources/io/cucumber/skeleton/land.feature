#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: bkfd.feature

Scenario: Scenario 1 Run AUDI proccess and verify results on web for program land fileid pscu bin 467538
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	467538|	/xfer/TEST/automation/land/LAND_467538.txt|/ncrp/LAND/LAND/WD/land.land.demo.visadps.in|
		And I modify the housekeeping containing the given characters in the given file
	    | HK | File |
      |  467538HKG |/ncrp/LAND/LAND/WD/land.land.demo.visadps.in|	
    When I run runbook "/xfer/TEST/automation/land/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/land/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Peter	|
	  | lastName			|	Lee	|
	  | address1			|	250 S NAPERVILLE RD|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
    | lastName			|	Ramsay	|
    | firstName     | Trevor |
		| unique user		|	B		|
		| status			|	A	|
		| address1			|	251 S NAPERVILLE RD|
	  | address2|APT 2|
	#	| conversionRatio			|	0.0120	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
				| lastName			|	Gursahani	|
		| firstName     | Joan   |
		| address1			|	253 S NAPERVILLE RD|
	  | address2			|	APT 4	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		
		| lastName			|	Kennedy	|
		| firstName     | Albert    |
		| address1			|	256 S NAPERVILLE RD|
	  | address2			|	APT 7	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
		| address1			|	258 S NAPERVILLE RD|
	  | address2			|	APT 9	|
	#	| conversionRatio			|	0.0120	|
		| lastName			|	Wong	|
		| firstName     | Joe  |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	J		|
		| status			|	X	|
		| address1			|	2800 S. Mill Avenue|
	  
	#	| conversionRatio			|	0.0120	|
		| lastName			|	Bani	|
		| firstName     | Liz  |
	#	| ptsExpirePeriod			|	4	|
	
	
	
#
#
Scenario: Scenario 2 Run AUDI proccess and verify results on web for program land fileid pscu bin 467538
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	486216|	/xfer/TEST/automation/land/LAND_486216.txt|/ncrp/LAND/LAND/WD/land.land.demo.visadps.in|
		  And I modify the housekeeping containing the given characters in the given file
	    | HK | File |
      |  486216HKG |/ncrp/LAND/LAND/WD/land.land.demo.visadps.in|	
			
    When I run runbook "/xfer/TEST/automation/land/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/land/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Jim|
	  | lastName			|	Jackson	|
	  | address1			|	1147 BRIARBROOK DR |
	  | address2			|	APT 1|
		| status			|	A	|
    And verified on getAccountApi
    | lastName			|	Barkley	|
    | firstName     | William  |
		| unique user		|	B		|
		| status			|	A	|
		| address1			|	1148 BRIARBROOK DR |
	  | address2			|	APT 2|
	#	| conversionRatio			|	0.0120	|
	
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
				| lastName			|	Walters	|
		| firstName     | Jack   |
	#	| ptsExpirePeriod			|	4	|
	| address1			|	1150 BRIARBROOK DR |
	| address2			|	APT 4|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| lastName			|	Morse	|
    | firstName     | John  |
		| status			|	L	|
		| lastname | Morse|
	#	| conversionRatio			|	0.0120	|
	| address1			|	1153 BRIARBROOK DR |
	| address2			|	APT 7|
		
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| lastName			|	Farley	|
		| firstName     | Peter  |
		| address1			|	1155 BRIARBROOK DR |
	| address2			|	APT 9|
	#	| ptsExpirePeriod			|	4	|
	And I dispose of the ssh
	
	