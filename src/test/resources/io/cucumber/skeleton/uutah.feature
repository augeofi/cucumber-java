#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: uutah.feature

Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	400011011|	/xfer/TEST/automation/ucuu/uutah/uutah_autoDemo.txt|/ncrp/UCUU/UUTAH/WD/ucuu.uutah.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/ucuu/uutah/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/ucuu/uutah/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Alberta	|
	  | lastName			|	Albertson	|
	  | address1			|	1620 WARRENVILLE ROAD|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|	1621 WARRENVILLE ROAD|
	  | address2			|	APT 2	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| firstName  | Samuel |
		| lastName			|	Robertson	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Almora	|
		| firstName     | Fred    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152107	|
		| lastName			|	Schwaber	|
		| firstName     | Kerry    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152109	|
		| lastName			|	Hendricks	|
		| firstName     | Henry  |
	#	| ptsExpirePeriod			|	4	|
	
	

	And I dispose of the ssh
	
	