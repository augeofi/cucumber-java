#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: holy.feature

Scenario: Run AUDI proccess and verify results on web for program uutah of ucuu
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	441535|	/xfer/TEST/automation/holy/demo/holy_demo.txt|/ncrp/HOLY/HOLY/WD/holy.holy.demo.client.in|
			
    When I run runbook "/xfer/TEST/automation/holy/demo/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/holy/demo/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Rob	|
	  | lastName			|	Burton	|
	  | address1			|	360 THIRD STREET|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
		| address1			|	361 THIRD STREET|
	  | address2			|	APT 1	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	5634152102	|
		| firstName  | Ram |
		| lastName			|	Patel	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	5634152104	|
		| lastName			|	Gursahani	|
		| firstName     | Anil   |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	5634152107	|
		| lastName			|	Coyote	|
		| firstName     | Willie    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	5634152109	|
		| lastName			|	Fonda	|
		| firstName     | Bill |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	L		|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| lastName			|	Alreja	|
		| firstName     | Ramesh |
	#	| ptsExpirePeriod			|	4	|
	  | address1			|	490 WESTFIELD ROAD|
	  |city | HOLYOKE|
	
	

	And I dispose of the ssh
	
	