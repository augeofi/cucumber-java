Feature: Audi_WebFeature

#####################################################
# Demo file has users with Card format: 6 dig bin, 4 digits of extended bin + 5 digits of unique alpha numeric + 1 alphanumeric we use to reference
#			 6 dig bin, 4 digits of extended bin + 5 zeros ( 00000 ) + 1 alphanumeric we use to reference in this test
#			 If there is no extended bin put zeros there
#		This program translates that to
#			 6 dig bin, 4 digits of extended bin + 5 digits of unique alpha numeric + 1 alphanumeric we use to reference
#
# BBBBBBEEEEAAAAAT. B: bin, E, extended bin, A unique alphanumeric, T, id for test
# we can reference card by teh last char 
#		"P" maps to BBBBBBEEEEAAAAAP because it ends in P
#		"C" maps to BBBBBBEEEEAAAAAC because it ends in C
#
# Unique Alphanumeric: example: ngf3nx
#
#  00ELEG000000000P
#  00ELEG000000000C
#  00ELEG000000000L
#  00ELEG000000000S

#  1234567890####
#  123456123412345
#  The data file is now available in /xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS.txt
#  https://augeofi.atlassian.net/browse/FL-2737
#
#
#
# runbookGherkinAuto.app1.qa.txt
# /opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.dataserver.qa.txt				99		app1
#
#431810jzm2q8
#1234567890123456
#    And I replace the given BIN in the given file: 
#    	|	BIN 	| 	File 	| 	Moveto 	|
#  		|	49077310	|	/xfer/TEST/automation/dow/dowDemo.txt	|	/ncrp/DOW/DOWC/WD/dow.dowc.demo.cif.in	|
 




  Scenario: Run AUDI proccess and verify results on web
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
  		|	49077310	|	/xfer/TEST/automation/dow/dowDemo.txt	|	/ncrp/DOW/DOWC/WD/dow.dowc.demo.cif.in	|
    When I run runbook "/xfer/TEST/automation/dow/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/dow/runbookDS.bash" from dataserver
		Then  Print Results Header
		And verified not on getAccountApi
		| unique user		|	H		|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Frederick	|
	  | lastName			|	Peterson	|
	  | address1			|	1620 PEACEFUL AVENUE	|
	  | address2			|	APT 5|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	D		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| firstName			|	Bert	|
	#	| ptsExpirePeriod			|	4	|
		And I dispose of the ssh



