Feature: This test verifies that an account is successfully created using householded

  Scenario: Run AUDI proccess and verify demographic results on web for FI tbma with version 2
    Given The SSH connection is setup
    
    #And I move the given file
      #| File                                               | Moveto                                       |
      #| /xfer/TEST/automation/tbma/tbma0/demo/tbma_demo.in | /ncrp/TBMA/TBMA0/WD/tbma.tbma0.demo.client.in |
    
    #When I run runbook "/xfer/TEST/automation/tbma/tbma0/demo/runbookApp.bash" from app1
    And I run runbook "/xfer/TEST/automation/tbma/tbma0/demo/runbook_tbma.bash" from dataserver
    
    
    
    Then Print Results Header
    
    And verified on getAccountApiRecyclable
      | unique user | 0001                    |
      | firstName   | Jack                   |
      | lastName    | Smith                    |
      | address1    | 1745 T Street Southeast |
      | address2    | Apartment 1             |
      | status      | A                       |
    


    
    Then I dispose of the ssh
    
    Scenario: Run AUDI proccess and verify demographic results on web for FI tbma with version 2.1 and
    address with 36 characters
    
    Given The SSH connection is setup
    
    And I run runbook "/xfer/TEST/automation/tbma/tbma0/demo/runbook_tbma_2.1.bash" from dataserver
    
    
    
    Then Print Results Header
    
    And verified on getAccountApiRecyclable
      | unique user | 2101                    |
      | firstName   | CARLOS                   |
      | lastName    | CONTRERAS                    |
      | address1    | 3034 EMPIRE STATE NORTH SEASHORE AVE |
      | address2    | SUITE 1001 OVERLOOKING ATLANTIC SEAS |
      | status      | A                       |
    


    
    Then I dispose of the ssh
