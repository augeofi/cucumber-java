Feature: land.feature

  Scenario: Run AUDI proccess and verify results on web for program land
    Given The SSH connection is setup
    
    And I move the given file
      | File                                            | Moveto                                       |
      | /xfer/TEST/automation/land/land/LAND_467538.txt | /ncrp/LAND/LAND/WD/land.land.demo.visadps.in |
    
    When I run runbook "/xfer/TEST/automation/land/land/runbookApp.bash" from app1
    And I run runbook "/xfer/TEST/automation/land/land/runbookDS.bash" from dataserver
    
    Then Print Results Header
    
    And verified on getAccountApiRecyclable
      | unique user | 5A                  |
      | firstName   | Peter               |
      | lastName    | Lee                 |
      | address1    | 250 S NAPERVILLE RD |
      | address2    | APT 1               |
      | status      | A                   |
    
    And verified on getAccountApiRecyclable
      | lastName    | Ramsay              |
      | firstName   | Trevor              |
      | unique user | 5B                  |
      | status      | A                   |
      | address1    | 251 S NAPERVILLE RD |
      | address2    | APT 2               |
    
    Then purge the users for pid "/xfer/TEST/automation/land/land/runGenPurgeForLANDCopy.bash" added today
    
    Then I dispose of the ssh
