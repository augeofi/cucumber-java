#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: bkfd.feature

Scenario: Run AUDI proccess and verify results on web for program bkfd fileid pscu bin 471213
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	471213|	/xfer/TEST/automation/bkfd/bkfd/pscu/bkfd_pscuBin1.txt|/ncrp/BKFD/BKFD/WD/bkfd.bkfd.demo.pscu.in|
			
    When I run runbook "/xfer/TEST/automation/bkfd/bkfd/pscu/runbookAppBin1.bash" from app1
    When I run runbook "/xfer/TEST/automation/bkfd/bkfd/pscu/runbookDSBin1.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	William	|
	  | lastName			|	Wong	|
	  | address1			|	802 WASHINGTON AVENUE|
	  | address2			|	APT 1A	|
		| status			|	A	|
    And verified on getAccountApi
    | lastName			|	Akiyama	|
		| unique user		|	B		|
		| status			|	A	|
		| address1			|	803 WASHINGTON AVENUE|
	  | address2			|	APT 1B	|
	#	| conversionRatio			|	0.0120	|
		| firstName  | Akemi |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
				| lastName			|	Barkley	|
		| firstName     | Adams    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		
		| lastName			|	Backus	|
		| firstName     | Eden    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| lastName			|	Denver	|
		| firstName     | Charles  |
	#	| ptsExpirePeriod			|	4	|
	
	

	And I dispose of the ssh
	
	