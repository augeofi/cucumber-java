#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: motn.feature

Scenario: Run AUDI proccess and verify results on web for program motn of motn
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|BIN|File|Moveto 	|
		|414066|/xfer/TEST/automation/elga/elga/demo/elgademo_bin1.txt|/ncrp/ELGA/ELGA/WD/elga.elga.demo.client1.in|
    When I run runbook "/xfer/TEST/automation/elga/elga/demo/runbookAppBin1.bash" from app1
    When I run runbook "/xfer/TEST/automation/elga/elga/demo/runbookDSBin1.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Sunil	|
	  | lastName			|	Gursahani	|
	  | address1			|	1040 BUENA VISTA DR.|
	  | address2			|	APARTMENT 1	|
		| status			|	A	|
    


	And I dispose of the ssh
	
	