#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: tmg.feature

Scenario: Run AUDI proccess and verify results on web for program 0682 of tmg
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		|	537958100	|	/xfer/TEST/automation/tmg/0682/tmg_0682_demo.txt	|	/ncrp/TMG/0682/WD/0682.demo.cif.in	|
    When I run runbook "/xfer/TEST/automation/tmg/0682/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/tmg/0682/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Samuel	|
	  | lastName			|	Adams	|
	  | address1			|	1620 OAKPARK AVENUE|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| lastName			|	Robertson	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Almora	|
		| firstName     | Fred    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	G		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152107	|
		| lastName			|	Schwaber	|
		| firstName     | Kerry    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152109	|
		| lastName			|	Hendricks	|
		| firstName     | Henry  |
	#	| ptsExpirePeriod			|	4	|
	
	
Scenario: Run AUDI proccess and verify results on web for program 087b of  
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		|	532028050	|	/xfer/TEST/automation/tmg/087b/tmg_087b_demo.txt	|	/ncrp/TMG/087B/WD/tmg.087b.demo.cif.in	|
    When I run runbook "/xfer/TEST/automation/tmg/087b/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/tmg/087b/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	Z	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Ram	|
	  | lastName			|	Patel |
	  | address1			|	1620 OAKPARK AVENUE|
	  | address2			|	APT 1	|
		| status			|	A	|
    And verified on getAccountApi
		| unique user		|	B		|
		| status			|	A	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152102	|
		| lastName			|	Morse	|
		| firstName			|	John	|
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	D		|
		| status			|	C	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152104	|
		| lastName			|	Farley	|
		| firstName     | Peter   |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	F		|
		| status			|	L	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152106	|
		| lastName			|	Coyote	|
		| firstName     | Willie    |
	#	| ptsExpirePeriod			|	4	|
	
	And verified on getAccountApi
		| unique user		|	I		|
		| status			|	X	|
	#	| conversionRatio			|	0.0120	|
		| phone			|	6304152109	|
		| lastName			|	Stewart	|
		| firstName     | Don  |
	#	| ptsExpirePeriod			|	4	|
	And I dispose of the ssh
	
	

	And I dispose of the ssh
	
	