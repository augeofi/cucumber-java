Feature: Audi_WebFeature

#####################################################
# Demo file has users with Card format: 6 dig bin, 4 digits of extended bin + 5 digits of unique alpha numeric + 1 alphanumeric we use to reference
#			 6 dig bin, 4 digits of extended bin + 5 zeros ( 00000 ) + 1 alphanumeric we use to reference in this test
#			 If there is no extended bin put zeros there
#		This program translates that to
#			 6 dig bin, 4 digits of extended bin + 5 digits of unique alpha numeric + 1 alphanumeric we use to reference
#
# BBBBBBEEEEAAAAAT. B: bin, E, extended bin, A unique alphanumeric, T, id for test
# we can reference card by teh last char 
#		"P" maps to BBBBBBEEEEAAAAAP because it ends in P
#		"C" maps to BBBBBBEEEEAAAAAC because it ends in C
#
# Unique Alphanumeric: example: ngf3nx
#
#  00ELEG000000000P
#  00ELEG000000000C
#  00ELEG000000000L
#  00ELEG000000000S

#  1234567890####
#  123456123412345
#  The data file is now available in /xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS.txt
#  https://augeofi.atlassian.net/browse/FL-2737
#
#
#
# runbookGherkinAuto.app1.qa.txt
# /opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.dataserver.qa.txt				99		app1
#
#431810jzm2q8
#1234567890123456
#    And I replace the given BIN in the given file: 
#    	|	BIN 	| 	File 	| 	Moveto 	|
#  		|	431810000	|	/xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS2_AUTOMATION.txt	|	/ncrp/TMG/1220/WD/tmg.1220.demo.cif.in	|
 



  Scenario: Run AUDI proccess and verify results on web
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
  		|	49077310	|	/xfer/TEST/automation/dow/dowDemo.txt 	|	/ncrp/DOW/DOWC/WD/dow.dowc.demo.cif.in	|
    When I run runbook "/xfer/TEST/automation/dow/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/dow/runbookDS.bash" from dataserver
		Then  Print Results Header
		And verified on getAccountApi
		| unique user		|	A		|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Frederick	|
	  | lastName			|	Peterson	|
	  | address1			|	1620 PEACEFUL AVENUE	|
	  | address2			|	APT 5	|
		| status			|	A	|
		| phone			|	6304152101	|
     
		And I dispose of the ssh



#  Scenario: Run AUDI proccess and verify results on web AVAD for avadian
 #   Given The SSH connection is setup
 #   And I replace the given BIN in the given file: 
#    	|	BIN 	| 	File 	| 	Moveto 	| 	
#  		|	410478000	|	 /xfer/TEST/automation/avad/avad/avad_demo.txt	|	/ncrp/AVAD/AVAD/WD/avad.avad.demo.visadps.in	|
#    When I run runbook "/xfer/TEST/automation/avad/avad/runbookApp.bash" from app1
#    When I run runbook "/xfer/TEST/automation/avad/avad/runbookDS.bash" from dataserver
#		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	G		|
#		And verified on getAccountApi
#		| unique user		|	A		|
#	  | firstName			|	Firstname	|
#		And I dispose of the ssh








####OLD

#  Scenario: Run AUDI proccess and verify results on web
#    Given The SSH connection is setup
#    And I replace the given BIN in the given file: 
#    	|	BIN 	| 	File 	| 	Moveto 	|
#  		|	430082000	|	/xfer/parm/udl/osu/demo/client/data/PRC185.RWCARDS2_AUTOMATION.txt	|	/ncrp/OSU/WD/osu.demo.client.in	|
##  		|	431810000	|	/xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS2_AUTOMATION.txt	|	/ncrp/OSU/WD/osu.demo.client.in	|
#    When I run runbook "/opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.app1.qa.txt" from app1
#    When I run runbook "bash /xfer/TEST/automation/runRunbook.bash" from dataserver
##    When I run runbook "runDataserverScript.txt2 /xfer/TEST/automation/runbookGherkinAuto.dataserver.qa.txt_new3" from dataserver
##   When I run runbook "/xfer/TEST/automation/runbookGherkinAuto.dataserver.qa.txt_new3" from dataserver
#    Then I should see the following user on the web
#		| unique user		|	D		|
#		| Parent			|	OSU	|
#	And I dispose of the ssh



#	430082000	A
#	430082000	B
# 431800000	C
# 430082000YE1XAOD
# 430082000YE1XAOE
# 430082000YE1XAOF
# 430082000YE1XAOG

#
#
#  Scenario: Run AUDI proccess and verify results on web
#    Given The SSH connection is setup
#    And I replace the given BIN in the given file: 
#    	|	BIN 	| 	File 	| 	Moveto 	|
#  		|	431810	|	/xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS2_AUTOMATION.txt	|	/ncrp/CCCU/CCCU/WD/cccu.cccu.demo.visadps.in	|
#    When I run runbook "/opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.qa.txt"
#    Then I should see the following user on the web
#		| unique user		|	C		|
#		| Parent			|	00ELEG	|
#	And I dispose of the ssh
#
#
#  Scenario: Run AUDI proccess and verify results on web
#    Given I replace the given BIN in the given file: 
#    	|	BIN 	| 431810 |
#  		| 	file 	| “/xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS.txt” |
#  		| 	Moveto 	| “/ncrp/CCCU/CCCU/WD/cccu.cccu.demo.visadps.in” |
#    When I run runbook "/opsman/cbl/parm/udl/osu/sub/demo/client/data/runbook.sh"
#    Then I should see the following user on the web
#		| unique user		|	C		|
#		| Parent			|	00ELEG	|


# ORIG
#  Scenario: Run AUDI proccess and verify results on web
#    Given I replace the given BIN in the given file: 
#    	|	BIN 	| 00ELEG |
#  		| 	file 	| “/opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_sample.txt” |
#    When I run runbook "/opsman/cbl/parm/udl/osu/demo/client/data/runbook.sh"
#    Then I should see the following user on the web
#		| unique user		|	C		|
#		| Parent			|	00ELEG	|
#
#
#
#
#
#
#
#
#
#
#
#  Scenario: Fun AUDI proccess and verify results on web
#    Given I replace "BIN_123456" with "BIN" and a unique timestamped alphanumeric code 
#    When I run runbook "/opsman/cbl/parm/udl/osu/sub/demo/client/data/runbook.sh"
#    Then I should see the following user on the web
#		| unique user		|	C		|
#		| Parent			|	00ELEG	|
		

## WORKS
#  Scenario: Audo formatted transaction output exact match to previous run 
#    Given I have staged file
#      | from		| /opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_destination.txt |
#      | to 			|/opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_source.txt|
#    When I run the audi process with parm file "parm file location"
#    Then System command "dir"
    



#  Scenario: Audo formatted transaction output exact match to previous run 
#    Given I have staged file
#      | to 			| from 			|
#      | /opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_sample.txt | /opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_sample.txt |
#    When I run the audi process with parm file "parm file location"
#    #Then diff for file "/ncrp/dir/dir/dir/temp.in" should show no differences
#    #Then Grep at line 50 should contain "MerchantNameDan"
#    Then System command "dir"
    
#####################################################################################################################



# Scenario: “OSU BIN TYPE is ‘D’ and PIN Data Present is ‘N’ and Network ID is ‘VSN’ has Transaction of DSP”
# GIVEN I have file staged at location “/opsman/cbl/parm/udl/osu/sub/demo/client/data/OSU_demo_sample.txt” to move to processing location “/ncrp/OSU/SUB/WD/osu.sub.demo.client.in”
# AND I have file at location "X"
# AND card number 1234567890_____1 changed to 1234567890TEST05
# AND Parm file is staged from “/opsman/cbl/parm/udl/osu/demo/*” to “/xfer/parm/udl/osu/demo/.”
# WHEN I run the audi process with parm file “/xfer/parm/udl/osu/demo/.”
# When I run the audi process with parm file "parm file location"
# THEN record 7 has into name/value pairs “Transaction” is equal to “DSP”
#Then System command "dir"


#####################################################################################################################



# We are starting with 2 basic paths.
# 1) AUDI file to web
#	    start with a AUDI file with a few basic rows and card numbers of REPLACE_THIS_CC#01,  REPLACE_THIS_CC#01   
#	use a function to replace those var with BIN + unique base 36 block encoding time in seconds
#   stage AUDI files
# run AUDI process
# hit web api (inspector) that looks up user data based on CC# 
# 2) AUDI file to output file.
# Here is a comment, 
# Comment #2    
# Scenario: Audo formatted transaction output exact match to previous run 
# Given I have file at location "X"
# When I run the audi process with parm file "parm file location"
# Then diff for file "/ncrp/dir/dir/dir/temp.in" should show no differences
# Then Grep at line 50 should contain "MerchantNameDan"
# Then System command "dir"
#  Scenario: a few cukes
#    Given I have 42 cukes in my belly
#    When I wait 1 hour
#    Then my belly should growl
