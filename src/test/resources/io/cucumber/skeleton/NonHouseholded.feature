#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: NonHouseholding.feature

Scenario: Run AUDI proccess and verify results on web for program land fileid pscu bin 4078682000
    Given The SSH connection is setup
    And I replace the given BIN in the given file: 
    	|	BIN 	| 	File 	| 	Moveto 	|
		  |	4078682000|	/xfer/TEST/automation/tmg/6969/demo/tmg_6969_demo.txt |/ncrp/TMG/6969/WD/6969.demo.cif.in|
			
    When I run runbook "/xfer/TEST/automation/tmg/6969/demo/runbookApp.bash" from app1
    When I run runbook "/xfer/TEST/automation/tmg/6969/demo/runbookDS.bash" from dataserver
		Then  Print Results Header
#		And verified not on getAccountApi
#		| unique user		|	J	|
		And verified on getAccountApi
		| unique user		|	A		|
	  | firstName			|	Rikki	|
	  | lastName			|	Albertina|
	  | address1			|	1745 T STREET SOUTHEAST|
	  | address2			|	APARTMENT 1	|
		| status			|	A	|
		
		And verified on getAccountApi
		| unique user		|	B		|
	  | firstName			|	Veloria	|
	  | lastName			|	Norty|
	  | address1			|	6007 APPLEGATE LANE|
	  | address2			|	APARTMENT 1	|
		| status			|	A	|
		
		And verified on getAccountApi
		| unique user		|	D	|
		| status			|	C	|
		
		And verified on getAccountApi
		| unique user		|	D	|
		| status			|	C	|
		
		And verified on getAccountApi
		| unique user		|	G	|
		| status			|	L	|
		
And verified on getAccountApi
		| unique user		|	H	|
		| status			|	L	|
	
And verified on getAccountApi
		| unique user		|	I	|
		| status			|	X	|
		
And verified on getAccountApi
		| unique user		|	J	|
		| status			|	X	|
		
And verified on getAccountApi
		| unique user		|	K	|
		| status			|	X	|
		
And verified on getAccountApi
		| unique user		|	L	|
		| status			|	X	|
		
And verified on getAccountApi
		| unique user		|	M|
		| status			|	X	|
		
And verified on getAccountApi
		| unique user		|	N		|
	  | firstName			|	Carlota	|
	  | lastName			|	Umesh |
		| status			|	A	|
		
		
		
    
   And I dispose of the ssh
    