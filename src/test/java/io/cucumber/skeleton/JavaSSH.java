package io.cucumber.skeleton;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class JavaSSH {

	Session session;
	//Channel channel;

	public JavaSSH (String user, String host, String password) {

		try {
			System.out.println("SSH Connecting. Host: " + host + " user: " + user + " password: " + password);
			JSch jsch = new JSch();
			session = jsch.getSession(user, host, 22);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(password);
			session.connect();
			boolean connected = session.isConnected();
			System.out.println("Session is Connected "+connected);
		} catch (Exception e) {

		}
	}

	public String ExecuteCommand(String command) throws Exception {
		boolean connect = session.isConnected();
		System.out.println("In execute session connected : "+connect);
		String commandResult;
		commandResult = " ";
		System.out.println("The command is " + command);

		Channel channel = session.openChannel("exec");
		boolean channelclosed = channel.isClosed();
		System.out.println("Channel is closed "+channelclosed);

		String username = session.getUserName();
		System.out.println("The username is "+username);
		String hostname = session.getHost();
		System.out.println("Hostname is "+hostname);
		
		((ChannelExec) channel).setCommand(command);
		boolean channeldisc = channel.isConnected();
		System.out.println("channel is connected "+channeldisc);
		channel.setInputStream(null);

		((ChannelExec) channel).setErrStream(System.err);

		InputStream in = channel.getInputStream();

		channel.connect();
		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i <= 0)
					break;
				//commandResult = new String(tmp, 0, i - 1);
				commandResult = new String(tmp, 0, i-1);
				
				
			}
			if (channel.isClosed()) {
				System.out.println("exit-status: " + channel.getExitStatus());
				System.out.println("channel is closed");
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
				ee.printStackTrace();
			}
		}
		channel.disconnect();
		
		return commandResult;
		
	}

	public void Dispose() {

		try {
			System.out.println("Dispose");

			//channel.disconnect();
			session.disconnect();
			System.out.println("DONE");
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}
/*
	public void Execute(String command) {

		try {
			channel = session.openChannel("exec");

			((ChannelExec) channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			System.out.println("This is the command " + command);

			InputStream in = channel.getInputStream();
			channel.connect();

			byte[] tmp = new byte[1024];
			while (true) {
				int numBytes = in.available();

				System.out.println("This is the numBytes : " + numBytes);
				if (numBytes == 0) {
					break;
				}
				while (in.available() > 0) {
					System.out.println("I am in while command is : " + command);
					int nb = in.read(tmp, 0, 1024);
					System.out.println("This is i " + nb);
					if (nb < 0)
						break;
					String ns = new String(tmp, 0, nb);
					System.out.println("this is ns " + ns);
					// System.out.print(new String(tmp, 0, nb));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
		} catch (JSchException | IOException e) {

			e.printStackTrace();
		}

	}

	public void Execute1(String command) {
		try {
			channel = session.openChannel("exec");

			((ChannelExec) channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			System.out.println("This is the command " + command);

			InputStream in = channel.getInputStream();
			channel.connect();

			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if (in.available() > 0)
						continue;
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			// channel.disconnect();
			// session.disconnect();

		} catch (Exception e) {

		}
	}

	public void ChannelShellExample(String args1, String args2, String args3) {
		try {
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			ChannelShell channel = (ChannelShell) session.openChannel("shell");
			channel.setOutputStream(outputStream);
			PrintStream stream = new PrintStream(channel.getOutputStream());
			channel.connect();

			stream.println(args1);
			stream.flush();
			waitForPrompt(outputStream);

			stream.println(args2);
			stream.flush();
			waitForPrompt(outputStream);

			stream.println(args3);
			stream.flush();
			waitForPrompt(outputStream);

			// channel.disconnect();
			// session.disconnect();
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}
*/
	static public void waitForPrompt(ByteArrayOutputStream outputStream) throws InterruptedException {
		int retries = 5;
		for (int x = 1; x < retries; x++) {
			Thread.sleep(1000);
			if (outputStream.toString().indexOf("$") > 0) {
				System.out.print(outputStream.toString());
				outputStream.reset();
				return;
			}
		}
	}

	public void ChannelShellExecute(List<String> commands) {
		
		
		ChannelShell channel; 
		channel = null ;
		
	

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
			channel = (ChannelShell) session.openChannel("shell");
            }
            catch (JSchException js) {
            	String message = js.getLocalizedMessage();
            	System.out.println("Error opening channel - " + message);
            }
			channel.setOutputStream(outputStream);
			PrintStream stream;
			stream = null ;
			try {
			 stream = new PrintStream(channel.getOutputStream());
			}
			catch (IOException ioe) {
				String message = ioe.getLocalizedMessage();
				System.out.println("Error getting pring stream error " + message);
			}
			try {
			channel.connect();
			}
			catch (JSchException js) {
				String message = js.getLocalizedMessage();
            	System.out.println("Error channelconnect - " + message);
			}
			for (String command:commands) {
				stream.println(command);
				stream.flush();
				try {
				 waitForPrompt(outputStream);
				}
				catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}

			
			

			channel.disconnect();
			//session.disconnect();
		
		
	}
}
