package io.cucumber.skeleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.eclipsesource.json.JsonObject;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.skeletons.Belly;

/* Change history
 * Anil Gursahani - The function Util.uniqueCardOrig() was returning a five character base 36 code instead of 6 character base 36 code.
 * Modified the function Util.uniqueCardOrig to return a base36 string of at least 6 characters and then got the first six characters from the base36 string.
 * 
 */

public class StepDefinitions {
	
	
	String AudiFile=""; 
	JavaSSH sshDataserver; // Only dataserver can see ncrp dir
	JavaSSH sshApp1; // Only App1 has runbook
	
	@Given("The SSH connection is setup")
	public void the_ssh_connection_is_setup() throws Exception {
		System.out.println("In ssh connection is setup");
		sshApp1 = new JavaSSH("qaautomation","10.158.1.103","9nBF%0L6t1C5");
		//
		//sshApp1.Init("app1.qa.augeofi.net", "qaautomation","Op3rat1on$1");

		//sshApp1.Init("10.161.1.249", "qaautomation","Operat1on$21");
		//sshApp1.Init("10.158.1.103", "qaautomation","9nBF%0L6t1C5");
		
		sshDataserver = new JavaSSH("qaautomation","10.158.1.106","9nBF%0L6t1C5");
		//sshDataserver.Init("dataserver.qa.augeofi.net", "qaautomation","Op3rat1on$1");
		//sshDataserver.Init("10.161.1.99", "qaautomation","Operat1on$21");
		//sshDataserver.Init("10.158.1.106", "qaautomation","9nBF%0L6t1C5");
		System.out.println("This is my session : "+sshDataserver.toString());
		
		
		
	    
	}
		


	@Given("I have file at location {int}")
	public void i_have_file_at_location(int intIn) {
	    // Write code here that turns the phrase above into concrete actions
		//JavaSSH ssh = new JavaSSH();
	    System.out.println("I have file at location" + intIn);
	    AudiFile = intIn+"";
	}

	@Given("I have file at location {string} to move to processing location {string}")
	public void i_have_file_at_location_x_to_move_to_processing_location_y(String moveFrom, String moveFileTo) {

		System.out.println("Meta description : " +moveFrom + ": " + moveFileTo); 
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}


	
	//@Given("I have staged file")
	//public void i_have_staged_file(io.cucumber.datatable.DataTable dataTable) {
		   ////List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		   
		    //System.out.println("to grid: |"+ dataTable.cell(0, 1) + "|");
		   // System.out.println("from grid: |"+ dataTable.cell(1, 1) + "|");
		    //System.out.println("to: |"+ list.get(0).get("to") + "|");
		    //System.out.println("from: |"+ list.get(0).get("from") + "|");
		    //System.out.println("Username - " + list.get(0).get(0));
			//System.out.println("Password - " + list.get(1));  
		    // Write code here that turns the phrase above into concrete actions
		    // For automatic transformation, change DataTable to one of
		    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
		    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
		    // Double, Byte, Short, Long, BigInteger or BigDecimal.
		    // For other transformations you can register a DataTableType.
		    //throw new io.cucumber.java.PendingException();
	//}
	
	/*		@Given("I have file at location {string}")
			public void i_have_file_at_location(String AudiFileIn) {
			    // Write code here that turns the phrase above into concrete actions
				HTMLParser html = new HTMLParser();
				//html.UrlExample();
				Document document  = html.GetHtmlDocById("019584122");
				Elements links = document.select("tr[data-alpha=bal]");  
				System.out.println("Meta description : " + links.toString()); 
				System.out.println("|||||||||||||||||||||||||| "); 
				System.out.println(Util.uniqueCard("00ELEG", "0001")); 
				System.out.println("|||||||||||||||||||||||||| "); 
				JavaSSH ssh = new JavaSSH();
				ssh.Init();
				ssh.Execute("ls -ltr");
				ssh.Execute("cd /ncrp");
				ssh.Dispose();
				System.out.println("I have file at location" + AudiFileIn);
			    AudiFile = AudiFileIn;
			}
	*/
	
	
	
	/*
	 * We go to app server and sudo to opsman 
	 * 		sudo -su opsman
	 * Take the orig runbook and make a copy
	 * 		cp /opsman/cbl/parm/udl/osu/demo/client/runbook.qa.txt /opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.qa.txt
	 * Comment out (with #) everything we dont need
	 * 		nano /opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.qa.txt
	 * we comment out the git pull, the compile, and moving the sample file
	 * we make the runbook runable
	 * 		chmod 777 /opsman/cbl/parm/udl/osu/demo/client/runbookGherkinAuto.qa.txt
	 */
	

	// sshApp1; // Only App1 has runbook				sshDataserver; // Only dataserver can see ncrp dir

	
	
	@When("I run runbook {string} from app1")
	public void i_run_runbook_from_app1(String stringIn) throws Exception {
	    
		
		Path path = Paths.get(stringIn);
		//System.out.println("This is parent path "+path.getParent());
		
		System.out.println("This is filename "+path.getFileName());
		
		//sshApp1.ChannelShellExample("cd "+path.getParent(), "sudo -u opsman /bin/bash", "bash "+path.getFileName());
		
		String parentPath = path.getParent().toString();
	
		StringBuffer sbuf = new StringBuffer("cd ");
		sbuf.append(parentPath);
		String cdCommand = sbuf.toString();	
		String commandOpsman = "sudo -u opsman /bin/bash";
		String bashCommand = "bash ";
		bashCommand = bashCommand.concat(path.getFileName().toString());
		
		List <String> commands = new ArrayList<String>();
		System.out.println(bashCommand);
		commands.add(cdCommand);
		commands.add(commandOpsman);
		commands.add(bashCommand);
		
		sshApp1.ChannelShellExecute(commands);
		
		
		
	}
	
	
	@When("I run runbook {string} from dataserver")
	public void i_run_runbook_from_dataserver(String stringIn) {
		
		
	    // Write code here that turns the phrase above into concrete actions

		Path path = Paths.get(stringIn);
		
		System.out.println("This is filename "+path.getFileName());
		
		//sshApp1.ChannelShellExample("cd "+path.getParent(), "sudo -u opsman /bin/bash", "bash "+path.getFileName());
		
		String parentPath = path.getParent().toString();
	
		StringBuffer sbuf = new StringBuffer("cd ");
		sbuf.append(parentPath);
		String cdCommand = sbuf.toString();	
		String commandOpsman = "sudo -u opsman /bin/bash";
		String bashCommand = "bash ";
		bashCommand = bashCommand.concat(path.getFileName().toString());
		
		List <String> commands = new ArrayList<String>();
		System.out.println(bashCommand);
		commands.add(cdCommand);
		commands.add(commandOpsman);
		commands.add(bashCommand);
		
		sshDataserver.ChannelShellExecute(commands);
		
	}

	@When("I run runbook {string} from dataserver legacy")
	public void i_run_runbook_from_dataserverLegacy(String stringIn) {
	    // Write code here that turns the phrase above into concrete actions
	//	sshDataserver.Execute("sudo su opsman");
//sudo -u opsman /bin/bash -c 'bash /xfer/TEST/automation/runbookGherkinAuto.dataserver.qa.txt_new3
		//System.out.println("exec " + stringIn); 
		String command1 = "sudo -u opsman /bin/bash && bash " + stringIn;
		System.out.println( command1); 
		//sshDataserver.Execute("exec " + stringIn);
		//System.out.println("exec " + stringIn); 
		//sshDataserver.ChannelShellExample("sudo -u opsman /bin/bash", stringIn);
		//sshDataserver.Execute( command1);
	}


	@When("I run runbook {string} from dataserver orig")
	public void i_run_runbook_from_dataserver_orig(String stringIn) {
	    // Write code here that turns the phrase above into concrete actions
	//	sshDataserver.Execute("sudo su opsman");

		//System.out.println("exec " + stringIn); 
		String command1 = "sudo -u opsman /bin/bash && bash " + stringIn;
		System.out.println( command1); 
		//sshDataserver.Execute("exec " + stringIn);
		//System.out.println("exec " + stringIn); 
		//sshDataserver.ChannelShellExample("sudo -u opsman /bin/bash", stringIn);
		//sshDataserver.Execute( command1);
	}

	
	
	@When("I run runbook {string}")
	public void i_run_runbook(String stringIn) {
	    // Write code here that turns the phrase above into concrete actions
		//sshApp1.ExecuteCommand("exec " + stringIn);

		System.out.println("exec " + stringIn); 
		
	}


	@Then("verified on getAccountApi")
	//public void i_should_see_the_following_user_on_the_web(io.cucumber.datatable.DataTable dataTable) {
	public void verified_on_getAccountApi(List<List<String>> 	 dataTable) {	
	try { 
		    
			Map<String, String> mapTable = Util.TableDictionaryConverter(dataTable);
			
		        
		        
			String User1CC = uniqueCardUnique + mapTable.get("unique user");
			System.out.println(uniqueCardUnique);
			JsonObject items =  Util.getAccountApi(User1CC);
			//System.out.println("city: " + items.get("city").asString());
			
			
			//System.out.println("================================================================================================================================");
			//System.out.println("================================================================================================================================");
			//System.out.println("R E S U L TS				R E S U L TS				R E S U L TS				R E S U L TS	");
			System.out.println("================================================================================================================================");
			
			
			//conversionRatio. Should be: [0.0100]. Is: [0.0120], MATCH: 
			//123456789012345678901234567890123456789012345678901234567890
		     for (String key : mapTable.keySet()) {
		    	 if (key.toLowerCase().contains("API Debug")) {
		    			System.out.println("API returned: " + items.asString());
		    	 }else if (!key.toLowerCase().contains("unique user")) {
		    		 //String report = key + ". Should be: [" + mapTable.get(key) + "]. Is: ["	+ items.get(key).asString()	+ "], MATCH:";
		    		 //report = Util.completeWithWhiteSpaces(report, 60);     // pad so final true/false aligns
		    		 String expected = mapTable.get(key);
		    		 String actual = items.get(key).asString();
		    		 String report = key + ". Should be : [" + expected  + "]. Is : [" + actual + "], MATCH:";
		    		 
		    		 boolean isMatch;
		    		 //isMatch = mapTable.get(key).equals(items.get(key).asString());
		    		
		    		 isMatch = expected.equalsIgnoreCase(actual);
		    		 
		    		 
		    		 //report += mapTable.get(key).equals(items.get(key).asString());
		    		 report += isMatch;
		    		 System.out.println(report) ;
		    		 
		    		 Assert.assertTrue(isMatch);
		    		 
		    		 
		    	 }
		           
		     }
		 	System.out.println("================================================================================================================================");
			

		}catch(Exception e){
			System.out.println( "i_should_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	}
	

	
	@Then("Print Results Header")
	public void Print_Results_Header() {
	try {
		    
			System.out.println("======================================================================================================================================");
			System.out.println("R E S U L TS				R E S U L TS				R E S U L TS				R E S U L TS	");
			System.out.println("======================================================================================================================================");
	
		}catch(Exception e){
			System.out.println( "Print_Results_Header *ERROR* : " + e.toString()   );
		}
	}
	
	@Then("verified not on getAccountApi")
	public void verified_not_on_getAccountApi(List<List<String>> 	 dataTable) {
		
	try {
		    
			Map<String, String> mapTable = Util.TableDictionaryConverter(dataTable);
			
			String User1CC = uniqueCardUnique + mapTable.get("unique user");
			//JsonObject items =  Util.getAccountApi(User1CC);
			String redirectURL = Util.GetRedirectNewUrl(User1CC);
			
			//System.out.println("city: " + items.get("city").asString());
			
			
			
			System.out.println("User \"" +  mapTable.get("unique user") + "\" not in system: " + redirectURL);
	
		 	System.out.println("================================================================================================================================");
			

		}catch(Exception e){
			System.out.println( "i_shouldnt_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	@Then("I should see the following user on getAccountApi legacy2")
	//public void i_should_see_the_following_user_on_the_web(io.cucumber.datatable.DataTable dataTable) {
	public void i_should_see_the_following_user_on_getAccountApi_legacy2(List<List<String>> 	 dataTable) {
		//List<List<String>> table	
	try {
		    
			Map<String, String> mapTable = Util.TableDictionaryConverter(dataTable);
			System.out.println("Status:  " + mapTable.get("status")  );
			
		  /* USEFUL DEBUG
			for (String key : mapTable.keySet())
		            System.out.println(key + " - " + mapTable.get(key));
		    */    
		        
		        
			String User1CC = uniqueCardUnique + mapTable.get("unique user");
			JsonObject items =  Util.getAccountApi(User1CC);
			//System.out.println("city: " + items.get("city").asString());
			
			
			System.out.println("================================================================================================================================");
			System.out.println("================================================================================================================================");
			System.out.println("R E S U L TS				R E S U L TS				R E S U L TS				R E S U L TS	");
			System.out.println("================================================================================================================================");
			
			
			//conversionRatio. Should be: [0.0100]. Is: [0.0120], MATCH: 
			//123456789012345678901234567890123456789012345678901234567890
		     for (String key : mapTable.keySet()) {
		    	 if (!key.toLowerCase().contains("unique user")) {
		    		 String report = key + ". Should be: [" + mapTable.get(key) + "]. Is: ["	+ items.get(key).asString()	+ "], MATCH:";
		    		 report = Util.completeWithWhiteSpaces(report, 60);     // pad so final true/false aligns
		    		 report += mapTable.get(key).equals(items.get(key).asString());
		    		 System.out.println(	report ) ;
		    	 }
		           
		     }
		 	System.out.println("================================================================================================================================");
			


		}catch(Exception e){
			System.out.println( "i_should_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	}
	
	
	
	
	@Then("I should see the following user on the web Legacy")
	//public void i_should_see_the_following_user_on_the_web(io.cucumber.datatable.DataTable dataTable) {
	public void i_should_see_the_following_user_on_the_web_Legacy(io.cucumber.datatable.DataTable 	 dataTable) {
	try {
			String User1CC = uniqueCardUnique + dataTable.cell(0, 1);
			System.out.println("WebVerify Parent: |"+ dataTable.cell(1, 1) + "|");
		    //System.out.println("WebVerify unique user: |"+ dataTable.cell(0, 1) + "|");
			//System.out.println("WebVerify uniqueCardOrig: |"+uniqueCardOrig + "|");
		    //System.out.println("WebVerify uniqueCardUnique: |"+uniqueCardUnique + userChar + "|");
			System.out.println("WebVerify uniqueCardUnique: |"+User1CC + "|");
			JsonObject items =  Util.getAccountApi(User1CC);
			//System.out.println("city: " + items.get("city").asString());
			//Assert.assertEquals( dataTable.cell(1, 1), items.get("city").asString(), items.get("city").asString());
		   // Util.SoupAccountInspector("019583366");  // this is a valid card in QA
		   // Util.SoupAccountInspector(uniqueCardUnique + "6");
		    //Util.SoupAccountInspector(uniqueCardUnique + "K");
		    //019583366 test user in QA
			//		    Util.SoupAccountInspector(uniqueCardUnique + userChar);
				  // List<Map<String, String>> list2 = dataTable.asMaps(String.class, String.class);
		}catch(Exception e){
			System.out.println( "i_should_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	}
	
	
	
@Given("I replace {string} with {string} and a unique timestamped alphanumeric code")
public void i_replace_with_and_a_unique_timestamped_alphanumeric_code(String string, String string2) {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}


	 @When("I run the audi process with parm file {string}")
	public void i_run_the_audi_process_with_parm_file(String parmFileLocation) {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("PARM: I have file at location" + parmFileLocation);
	    System.out.println("PARM: I have file at location" + AudiFile);
	    //throw new io.cucumber.java.PendingException();
	}

	
	@Then("diff for file {string} should show no differences")
	public void diff_for_file_should_show_no_differences(String stringin) {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new io.cucumber.java.PendingException();
	    System.out.println("DIFF, system a command: between: " + stringin);
	}
	

	
	// sshApp1; // Only App1 has runbook				sshDataserver; // Only dataserver can see ncrp dir

	//Takes 6 dig bin 3 dig extended bin (pads with zeros if less then 9 digits)  ==> 9 digits
	//  
	public static String uniqueCardOrig;
	public static String uniqueCardUnique;
	public static String base36Value;
	
	public static void storeBase36(String newBase36Value) {
	
		base36Value = newBase36Value;
	
	}
	
	public static String getLastBase36() {
		
		return base36Value;
	
	}
	

	
	@Given("I replace the given BIN in the given file")
	public void i_replace_the_given_bin_in_the_given_file(io.cucumber.datatable.DataTable dataTable) {
		try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    String strBin = list.get(0).get("BIN").replace("?", "");
			String strFile =  list.get(0).get("File").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
			String strBin9Dig = strBin; 
			String strBin15Dig = strBin ;
			System.out.println("Str bin is " + strBin);
			int len ;
			len = strBin.length();
			System.out.println ("Str bin length is "+ len);
			while(strBin.length() < 9) {
				strBin = strBin + "0";
			}
		    //while (strBin9Dig.length() < 9 ) strBin9Dig += "0";   // builds bin lengh to 9 char
		//	while (strBin9Dig.length() < 15 ) strBin9Dig += "0";   // builds bin lengh to 9 char
		    while(strBin15Dig.length() < 15) {
		    	strBin15Dig += "0" ;
		    }
			
			System.out.println("BIN: |"+ strBin + "|    File: |"+ strFile + "|    MoveTo: |"+ strMoveTo + "|");
			String moveCmd = "cp " + strFile + " " + strMoveTo;
//			System.out.println( moveCmd  );
			
			//sshDataserver.ChannelShellExample("sudo -u opsman /bin/bash", moveCmd);
			//sshDataserver.Execute(moveCmd );
			
	
			
		//	uniqueCardOrig = strBin15Dig;
			
			Util.obtainNewBase36();
			
			String base36ValueLocal= Util.retrieveBase36();
			//String base36ValueLocal = Util.uniqueCardOrig();
			System.out.println("BEFORE BASE36 Value: "+base36ValueLocal);
	
			System.out.println("AFTER BASE36 Value: "+base36ValueLocal);
			StringBuffer sb = new StringBuffer(base36ValueLocal);
			int numCharsToGet ;
			numCharsToGet = 15 - strBin.length();
			String base36ValueDiff = sb.substring(0, numCharsToGet); // base 36 string returned could be more than six characters, Get number of chars we need.
			
			
			
	//		uniqueCardUnique = strBin +  Util.uniqueCardOrig();
			uniqueCardUnique = strBin +  base36ValueDiff ;
			
			
;			
			uniqueCardUnique = uniqueCardUnique.toUpperCase();
			System.out.println("Unique card unique is " + uniqueCardUnique);
			
			
			//ORIG  String replaceBinCmd = "sed -i 's/" + strBin9Dig + "/" + uniqueCardUnique +"/g' " + strMoveTo;
			String replaceBinCmd = "sed -i -e 's/" + strBin15Dig + "/" + uniqueCardUnique +"/g' " + strMoveTo;
			
			//sed  -i -e 's/431810000000000/431810000Y7HCBX/g' /ncrp/OSU/WD/osu.demo.client.in
			
			
			System.out.println( replaceBinCmd  );
			sshDataserver.ExecuteCommand(replaceBinCmd);
		}catch(Exception e){
			System.out.println( "i_replace_the_given_bin_in_the_given_fileIsTHIS *ERROR* : " + e.toString()   );
		}
		
	}
	
	
	@Given("I have the given NonCard file")
	public void i_have_the_given_non_card_file(io.cucumber.datatable.DataTable dataTable) {
		System.out.println("This is from NONCARD&&&");
		try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
			String strFile =  list.get(0).get("File").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
			String moveCmd = "cp " + strFile + " " + strMoveTo;
			System.out.println( moveCmd  );
			sshDataserver.ExecuteCommand(moveCmd );
		}catch(Exception e){
			System.out.println( "i_have_given_noncard *ERROR* : " + e.toString()   );
		}

	}
	
//Remove below
	/*@Given("I modify the given HKG in the given file")
	public void i_modify_the_given_hkg_in_the_given_file(io.cucumber.datatable.DataTable dataTable) {
		try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    
		    String strHK =  list.get(0).get("HK").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
			String strFile = list.get(0).get("File").replace("?", "");
			
			System.out.println("StrHK is " + strHK);

			int hkLength = strHK.length();
			
			int hkIndex = strHK.indexOf("HK");
			
			StringBuffer hkSB = new StringBuffer(strHK);
				
			int numCharsToGet;
			numCharsToGet = hkLength - hkIndex;
            
            String base36Str ;
            base36Str = Util.uniqueCardOrig();
            base36Str = Util.retrieveBase36();
            System.out.println("***LASTBASE36***: "+base36Str);
            int base36StrLen ;
            base36StrLen = base36Str.length();
            StringBuffer base36SB = new StringBuffer(base36Str);
            int startIdx = base36StrLen - numCharsToGet ;
                 
            String base36Value = base36SB.substring(startIdx);
			hkSB.replace(hkIndex, hkLength, base36Value);
			System.out.println("Base36Value is***********************"+base36Value+"********************");
			String newHKStr = hkSB.toString().toUpperCase();
			String replaceCmd = "sed -i -e 's/" + strHK + "/" + newHKStr + "/' " + strMoveTo;
			System.out.println(replaceCmd);
						
			sshDataserver.Execute(replaceCmd);
		}catch(Exception e){
			System.out.println( "i_replace_the_given_bin_in_the_given_file *ERROR* : " + e.toString()   );
		}
		
		
	   
	}*/

	@When("I obtain the augeoID from user")
	public void i_obtain_the_augeo_id_from_user(io.cucumber.datatable.DataTable dataTable) {
	    try {
		  List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    
		    String lastName =  list.get(0).get("lastName").replace("?", "");
			String zip = list.get(0).get("zip").replace("?", "");
			String parent = list.get(0).get("parent").replace("?", "");
			
			URL url = new URL("http://cbssecure.qa.augeofi.net/scripts/cgirpc32.dll/dpsearch.cob");
			Map<String, Object> params = new LinkedHashMap<>();
	        params.put("WEBIN_ACTION", "get");
	        params.put("WEBIN_ZIP5", zip);
	        params.put("WEBIN_PARENT", parent);
	        params.put("WEBIN_NAME", lastName);

			Util.PostToCobtestUrl(url, params);	
	} catch (IOException e) {
			
		}
	    
	}
	
	
	
	
	@Given("I modify the housekeeping containing the given characters in the given file")
	public void i_modify_the_housekeeping_containing_the_given_characters_in_the_given_file(io.cucumber.datatable.DataTable dataTable){
		
		
		//System.out.println("BASE36 in HOUSEKEEPING FUNCTION" +getLastBase36());
		try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    
		    String strHK =  list.get(0).get("HK").replace("?", "");
			String strMoveTo = list.get(0).get("File").replace("?", "");
			
			System.out.println("StrHK is " + strHK);

			int hkLength = strHK.length();
			
			int hkIndex = strHK.indexOf("HK");
			
			StringBuffer hkSB = new StringBuffer(strHK);
			
			
			int numCharsToGet;
			numCharsToGet = hkLength - hkIndex;

			String base36Str = Util.retrieveBase36();
			System.out.println(base36Str);
			if (base36Str.length() == 0) {Util.obtainNewBase36();
			base36Str= Util.retrieveBase36();
			System.out.println("***base36str********"+base36Str);	
			}
    
            //Util.obtainNewBase36();
            //base36Str = Util.retrieveBase36();
            System.out.println("***LASTBASE36***: "+base36Str);
            int base36StrLen ;
            base36StrLen = base36Str.length();
            StringBuffer base36SB = new StringBuffer(base36Str);
            int startIdx = base36StrLen - numCharsToGet ;   
            
            String base36Value = base36SB.substring(startIdx);
			hkSB.replace(hkIndex, hkLength, base36Value);
			System.out.println("Base36Value is***********************"+base36Value+"********************");
			String newHKStr = hkSB.toString().toUpperCase();
			String replaceCmd = "sed -i -e 's/" + strHK + "/" + newHKStr + "/' " + strMoveTo;
			System.out.println(replaceCmd);
			
				
			sshDataserver.ExecuteCommand(replaceCmd);
		}catch(Exception e){
			System.out.println( "I modify the housekeeping containing the given characters in the given file *ERROR* : " + e.toString()   );
		} System.out.println("EXITING HOUSEKEEPING FUNCTION");
	}
	
	@Given("I test old card strategy using given BIN and file")
	public void i_test_old_card_strategy_using_given_bin_and_file(io.cucumber.datatable.DataTable dataTable) {
		System.out.println("************************CARD STRATEGY*******************");
		TestOldNewCardStrategy(dataTable);
	    
	}
	
	
	public void TestOldNewCardStrategy(io.cucumber.datatable.DataTable dataTable) {
	
		System.out.println("----------------TEST METHOD------------");
		try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    String strBin = list.get(0).get("BIN").replace("?", "");
			String strFile =  list.get(0).get("File").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
			String strBin9Dig = strBin; 
			String strBin15Dig = strBin ;
			System.out.println("Str bin is " + strBin);
			int len ;
			len = strBin.length();
			System.out.println ("Str bin length is "+ len);
			while(strBin.length() < 9) {
				strBin = strBin + "0";
			}
		    //while (strBin9Dig.length() < 9 ) strBin9Dig += "0";   // builds bin lengh to 9 char
		//	while (strBin9Dig.length() < 15 ) strBin9Dig += "0";   // builds bin lengh to 9 char
		    while(strBin15Dig.length() < 15) {
		    	strBin15Dig += "0" ;
		    }
			
			System.out.println("BIN: |"+ strBin + "|    File: |"+ strFile + "|    MoveTo: |"+ strMoveTo + "|");
			String moveCmd = "cp " + strFile + " " + strMoveTo;
			System.out.println( moveCmd  );
			sshDataserver.ExecuteCommand(moveCmd );
			
		//	uniqueCardOrig = strBin15Dig;
			
			
			
			
			base36Value = Util.retrieveBase36(); 
			StringBuffer sb = new StringBuffer(base36Value);
			int numCharsToGet ;
			numCharsToGet = 15 - strBin.length();
			base36Value = sb.substring(0, numCharsToGet); // base 36 string returned could be more than six characters, Get number of chars we need.
			System.out.println("THE BASE36 VALUE IS"+base36Value);
			
			
	//		uniqueCardUnique = strBin +  Util.uniqueCardOrig();
			uniqueCardUnique = strBin +  base36Value ;
			
			
;			
			uniqueCardUnique = uniqueCardUnique.toUpperCase();
			System.out.println("Unique card unique is " + uniqueCardUnique);
			
			
			//ORIG  String replaceBinCmd = "sed -i 's/" + strBin9Dig + "/" + uniqueCardUnique +"/g' " + strMoveTo;
			String replaceBinCmd = "sed -i -e 's/" + strBin15Dig + "/" + uniqueCardUnique +"/g' " + strMoveTo;
			
			//sed  -i -e 's/431810000000000/431810000Y7HCBX/g' /ncrp/OSU/WD/osu.demo.client.in
			
			
			System.out.println( replaceBinCmd  );
			sshDataserver.ExecuteCommand(replaceBinCmd);
		}catch(Exception e){
			System.out.println( "i_replace_the_given_bin_in_the_given_file *ERROR* : " + e.toString()   );
		}
		
	}
	
	
	//Takes 6 dig bin 3 dig extended bin (pads with zeros if less then 9 digits)  ==> 9 digits
	//  
	/*@Given("orig I replace the given BIN in the given file(orig):")
	public void i_replace_the_given_bin_in_the_given_file_orig(io.cucumber.datatable.DataTable dataTable) {
		  // List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		   //System.out.println("BIN: |"+ dataTable.cell(0, 1) + "|");
		   //System.out.println("File: |"+ list.cell(1, 1) + "|");
		    //System.out.println("height: |"+ list.height() + "|" + "height: |"+ dataTable.width() + "|" + dataTable + "|");

		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
		    
		    String strBin = list.get(0).get("BIN").replace("?", "");
			String strFile =  list.get(0).get("File").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
		    
		    
		    //String strBin = dataTable.cell(0, 1);
		    //String strFile = dataTable.cell(1, 1);
			//	String strMoveTo = "";
			//String strMoveTo = dataTable.cell(2, 1);
			String strBin9Dig = strBin; 
		    while (strBin9Dig.length() < 9 ) strBin9Dig += "0";   // builds bin lengh to 9 char
		   
		    System.out.println("BIN: |"+ strBin + "|    File: |"+ strFile + "|    MoveTo: |"+ strMoveTo + "|");
		    //System.out.println("BIN: |"+ strBin + "|" + "File: |"+ strFile + "|" + "MoveTo: |"+ strMoveTo + "|");
		    
		    
			//System.out.println("|||||||||||||||||||||||||| "); 
			//System.out.println(Util.uniqueCard("00ELEG", "0001")); 
			
		    
			// ssh = new JavaSSH();
		   // ssh.Init("dataserver.qa.augeofi.net", "testuser","lq6OGDWV8*GZ");
			String moveCmd = "cp " + strFile + " " + strMoveTo;
			System.out.println( moveCmd  );
			sshDataserver.Execute(moveCmd );
			String replaceBinCmd = "sed -i 's/" + strBin9Dig + "/" +strBin+ Util.uniqueCardOrig() +"/g' " + strMoveTo;
			//sed -i 's/431810000000000/43181AAAAAAAAAA/g' /xfer/parm/udl/cccu/cccu/demo/visadps/data/PRC185.RWCARDS2.txt
			System.out.println( replaceBinCmd  );
			sshDataserver.Execute(replaceBinCmd);

			//ssh.Execute("cd /ncrp");
			//ssh.Dispose();
	}*/
	// sshApp1; // Only App1 has runbook				sshDataserver; // Only dataserver can see ncrp dir

	
	@Then("I dispose of the ssh")
	public void i_dispose_of_the_ssh() {
	    // Write code here that turns the phrase above into concrete actions
		sshApp1.Dispose();
		sshDataserver.Dispose();
		}


	
	
	@Then("Grep at line {int} should contain {string}")
	public void diff_should_show_zero_differences(int lineIn, String stringmatch) {
	    System.out.println("GREP, system command line: " + lineIn + "     stringmatch: " + stringmatch);
	}

	@Then("System command {string}")
	public void system_command (String stringin) {
	    System.out.println("SYSTEM COMMAND: " + stringin);
		//String homeDirectory = System.getProperty("user.home");
	    
	    try {
	        Process process = Runtime.getRuntime().exec(stringin);
	     
	        BufferedReader reader = new BufferedReader(
	                new InputStreamReader(process.getInputStream()));
	        String line;
	        while ((line = reader.readLine()) != null) {
	            System.out.println(line);
	        }
	     
	        reader.close();
	     
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
//	    
//		boolean isWindows = true;
//		Process process;
//		if (isWindows) {
//		    try {
//				//process = Runtime.getRuntime().exec(String.format("cmd.exe /c dir"));
//				process = Runtime.getRuntime().exec(String.format("dir"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else {
//		    //process = Runtime.getRuntime().exec(String.format("sh -c ls %s", homeDirectory));
//		}
	}
	
	
	
    @Given("I have {int} cukes in my belly")
    public void I_have_cukes_in_my_belly(int cukes) {
        Belly belly = new Belly();
        belly.eat(cukes);
    }
	
    @When("I wait {int} hour")
	public void i_wait_hour(Integer int1)  {
		// Write code here that turns the phrase above into concrete actions     	//throw new io.cucumber.java.PendingException();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    @Then("^my belly should growl$")
    public boolean my_belly_should_growl() throws Throwable {
        return true;
    }


    //Genpurge code
    
    @When("I move the given file")
    public void i_move_the_given_file(io.cucumber.datatable.DataTable dataTable) {
    	try {
		    List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);

			String strFile =  list.get(0).get("File").replace("?", "");
			String strMoveTo = list.get(0).get("Moveto").replace("?", "");
			
			
			System.out.println("File: |"+ strFile + "|    MoveTo: |"+ strMoveTo + "|");
			String opsmanCommand = new String("sudo -u opsman /bin/bash");
			String cpCommand = "cp ";
			cpCommand = cpCommand.concat(strFile).concat(" ").concat(strMoveTo);
			String sedCommand = "sed -i -e 1,1s/YYYYMMDD/";
			sedCommand = sedCommand.concat(Util.getCurrentDate()).concat("/").concat(" ").concat(strMoveTo);
			System.out.println("The sedCommand is "+sedCommand);
			List <String> commands = new ArrayList<>();
			commands.add(opsmanCommand);
			commands.add(cpCommand);
			commands.add(sedCommand);
			sshDataserver.ChannelShellExecute(commands);
			
			//String moveCmd = "cp " + strFile + " " + strMoveTo;
			//System.out.println( moveCmd);
	
			

		}catch(Exception e){
			
			System.out.println("I_move_the_given_file *ERROR* : " + e.toString()   );
		}

 
    }

    @Then("verified on getAccountApiRecyclable")
    public void verified_on_get_account_api_recyclable(List<List<String>> dataTable) {

	try { 
		    
			Map<String, String> mapTable = Util.TableDictionaryConverter(dataTable);		        
		     
			String pattern = mapTable.get("unique user");
			

			
			
			
			String command = "ls -rt /PRODBankFiles/audi_reporting/* | tail -1";
			String audifile =  sshDataserver.ExecuteCommand(command);
			//sshDataserver.ExecuteCommand(command);
			System.out.println(audifile);
			StringBuffer sbuf = new StringBuffer("python -m  json.tool ");
		      sbuf.append(audifile + " | grep  mapper_output_file_path | cut -d: -f2");
		      
		      //sbuf.append(" | grep  mapper_output_file_path");
		      command = sbuf.toString();
		      String commandResult = sshDataserver.ExecuteCommand(command);
		      System.out.println("Command result is " + commandResult);
		      int startPath = commandResult.indexOf('"');
		      int endPath = commandResult.lastIndexOf('"');
		      String mapperOutputFilePath = commandResult.substring(startPath+1, endPath);
		      System.out.println("Maper output file path is " + mapperOutputFilePath);
		     // String pattern = "1A" ;
		      
		      command = "cut -c2-17 " ;
		      sbuf = new StringBuffer(command) ;
		      sbuf.append(mapperOutputFilePath + " | grep " + pattern + "$");
		      command = sbuf.toString();
		      String pan = sshDataserver.ExecuteCommand(command);
		      System.out.println("PAN is " + pan);
		      
			
			
			JsonObject items =  Util.getAccountApi(pan);
			
			System.out.println("================================================================================================================================");		
		
		     for (String key : mapTable.keySet()) {
		    	 if (key.toLowerCase().contains("API Debug")) {
		    			System.out.println("API returned: " + items.asString());
		    	 }else if (!key.toLowerCase().contains("unique user")) {
		    		 String expected = mapTable.get(key);
		    		 String actual = items.get(key).asString();
		    		 String report = key + ". Should be : [" + expected  + "]. Is : [" + actual + "], MATCH:";		    		 
		    		 boolean isMatch;
		    		 isMatch = expected.equalsIgnoreCase(actual);
		    		 report += isMatch;
		    		 System.out.println(report) ;		    		 
		    		 Assert.assertTrue(isMatch);
		    		 		    		 
		    	 }
		           
		     }
		 	System.out.println("================================================================================================================================");
			

		}catch(Exception e){
			System.out.println( "i_should_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	
    }
    @Then("verified on getAccountApiRecycle")
    public void verified_on_get_account_api_recycle(List<List<String>> dataTable) {
	
    	try { 
		    
			Map<String, String> mapTable = Util.TableDictionaryConverter(dataTable);		        
		    String card = mapTable.get("unique user");
			JsonObject items =  Util.getAccountApi(card);
			
			System.out.println("================================================================================================================================");		
		
		     for (String key : mapTable.keySet()) {
		    	 if (key.toLowerCase().contains("API Debug")) {
		    			System.out.println("API returned: " + items.asString());
		    	 }else if (!key.toLowerCase().contains("unique user")) {
		    		 String expected = mapTable.get(key);
		    		 String actual = items.get(key).asString();
		    		 String report = key + ". Should be : [" + expected  + "]. Is : [" + actual + "], MATCH:";		    		 
		    		 boolean isMatch;
		    		 isMatch = expected.equalsIgnoreCase(actual);
		    		 report += isMatch;
		    		 System.out.println(report) ;		    		 
		    		 Assert.assertTrue(isMatch);
		    		 		    		 
		    	 }
		           
		     }
		 	System.out.println("================================================================================================================================");
			

		}catch(Exception e){
			System.out.println( "i_should_see_the_following_user_on_the_web *ERROR* : " + e.toString()   );
		}
	
    }

    @Then("purge the users for pid {string} added today")
    public void purge_the_users_for_pid_added_today(String purge) {
    	
    	
//		String command1 = "sudo -u opsman /bin/bash";
//		System.out.println( "Command is " + command1); 
//		sshDataserver.Execute("exec " + command1);	
//    	
    	//sshDataserver.Execute( purge);
    	
    	//sshDataserver.ChannelShellExample("","sudo -u opsman /bin/bash ","bash " +purge);
    	
    	
		Path path = Paths.get(purge);
		//System.out.println("This is parent path "+path.getParent());
		
		System.out.println("This is filename "+path.getFileName());
		
		String opsmanCommand ="sudo -u opsman /bin/bash";
    	String cdCommand = "cd "+path.getParent();
    	String genPurgeCommand = "bash ".concat(path.getFileName().toString()).concat(" N");
    	System.out.println("The genpurge command is : "+genPurgeCommand);
    	System.out.println("The cd command is : "+cdCommand);
    	
		List <String> commands = new ArrayList<>();
		commands.add(opsmanCommand);
		commands.add(cdCommand);
		commands.add(genPurgeCommand);
		
		sshDataserver.ChannelShellExecute(commands);
		
		//sshApp1.ChannelShellExample("cd "+path.getParent(), "", "bash "+path.getFileName());
	
    }
    
    
    
    
}





//package io.cucumber.skeleton;

//import io.cucumber.java.en.Given;
//public class StepDefinitions {
 //   @Given("I have {int} cukes in my belly")
 //   public void I_have_cukes_in_my_belly(int cukes) {
 //       Belly belly = new Belly();
 //       belly.eat(cukes);
 //   }
//}

