package io.cucumber.skeleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

/* Change history
 * July 17, 2021
 * Use LongToString instead of IntegerToString in function UniqueCardOrig()
 */

public class Util {
	
	public static String getCurrentDate() {
		Date dNow = new Date( );
	     SimpleDateFormat ft = new SimpleDateFormat ("yyyyMMdd");
	     String dateFormat = ft.format(dNow);
	      //System.out.println("Current Date: " + ft.format(dNow));
	      return dateFormat;
	}

	static String base36 = "";

	/*
	 * 
	 * # CC# is 16 digits # 1234567890123456 # BIN (6 digits) TimeinMiliseconds #
	 * YYYYMMDDHHSS # 1234567890123456 <--- This is the length of a CC, 16 digits #
	 * 00ELEGYYMMDDHHSS <--- This is an example card # with BIN guarenteed to be
	 * unique for each second. # 1508484583259 # Time in milliseconds using Date
	 * class: 1508484583259 # Time in milliseconds using Calendar: 1508484583267 #
	 * Getting time in milliseconds in Java 8: 1508484583331
	 * 
	 */
	public static void obtainNewBase36() {
		base36 = uniqueCardOrig();

	}

	public static String retrieveBase36() {
		return base36;
	}

	public static String uniqueCard(String bin, String FourDig) {
		// here m will be in seconds
		long reducedInt = (System.currentTimeMillis() - 1497580000000L) / 1000;
		// long test = 1497580000000L; // definitely in the past
		// long test = 1500080000000L; // definitely in the past
		long curMS = System.currentTimeMillis();
		long pastMS = 1497580000000L;
		// long reducedInt = System.currentTimeMillis() - test;

		System.out.println("1234567890123456");
		System.out.println("ms since epoc: " + System.currentTimeMillis());
		System.out.println("System.currentTimeMillis() reducedInt: " + reducedInt);
		// System.out.println("System.currentTimeMillis() m - 1497580000000L: " + (m -
		// test));

		// System.out.println("currentTimeMillis reduced: " + (reducedInt));
		// System.out.println("currentTimeMillis reduced: " + (reducedInt));
		System.out.println("Mil time base 36 100000: " + Integer.toString((int) (100000), 36));
		System.out.println("Mil time base 36 100000000: " + Integer.toString((int) (100000000), 36));
		System.out.println("Mil time base 36 1497580000000L: " + Integer.toString((int) (pastMS), 36));
		System.out.println("Mil time base 36 currentTimeMillis: " + Integer.toString((int) (curMS), 36));
		System.out.println("Mil time base 36 : reducedInt : " + Integer.toString((int) (reducedInt), 36));

		long now = System.currentTimeMillis(); // Simpler way to get current time

		try {
			Date date = new SimpleDateFormat("mm dd yyyy h:mm:ss a").parse("07 15 2020 1:00:00 PM");
			long timeElapsed = now - date.getTime(); // Here's your number of ms
			System.out.println(
					"Mil time base 36 : " + Integer.toString((int) (timeElapsed), 36) + "   timeElapsed" + timeElapsed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Long startTime = Long.parseLong("1497674732168");
		Long endTime = Long.parseLong("1497574732168");
		System.out.println("start time is" + new Date(startTime) + " end time is " + new Date(endTime));
		// 1497580000000 as it's definitely in the past at the time I wrote this and its
		// a nice even number.

		System.currentTimeMillis();
		// 00ELEGYYMMDDHHSS

		// String bin = "00ELEG";
		String Base36CurrentTimeMSStr;
		Base36CurrentTimeMSStr = Integer.toString((int) System.currentTimeMillis(), 36);
		String binPlusTimeInMs = bin + Base36CurrentTimeMSStr;

//		String binPlusTimeInMs = bin + Integer.toString((int) System.currentTimeMillis(), 36);
		System.out.println("Bin is " + bin);
		System.out.println("binPlusTimeInMs is " + binPlusTimeInMs);
		System.out.println("FourDig is " + FourDig);
		String CC = binPlusTimeInMs + FourDig; // = binPlusTimeInMs + "0001";
		// System.out.println(binPlusTimeInMs); //00ELEGngf3nx
		return CC;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////

	public static String uniqueCardOrig(String bin, String FourDig) {
		// here m will be in seconds
		long m = System.currentTimeMillis() / 1000;
		long test = 1497580000000L; // definitly in the past

		long reducedInt = System.currentTimeMillis() - test;

		System.out.println("1234567890123456");
		// System.out.println("00ELEGYYMMDDHHSS");
		System.out.println("System.currentTimeMillis(): " + System.currentTimeMillis());
		System.out.println("System.currentTimeMillis() m: " + m);
		System.out.println("System.currentTimeMillis() m - 1497580000000L: " + (m - test));

		System.out.println("currentTimeMillis reduced: " + (reducedInt));
		System.out.println("currentTimeMillis reduced: " + (reducedInt));
		System.out.println("Mil time base 36 : " + Integer.toString((int) (reducedInt), 36));

		long now = System.currentTimeMillis(); // Simpler way to get current time

		try {
			Date date = new SimpleDateFormat("mm dd yyyy h:mm:ss a").parse("07 15 2020 1:00:00 PM");
			long timeElapsed = now - date.getTime(); // Here's your number of ms
			System.out.println(
					"Mil time base 36 : " + Integer.toString((int) (timeElapsed), 36) + "   timeElapsed" + timeElapsed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Long startTime = Long.parseLong("1497674732168");
		Long endTime = Long.parseLong("1497574732168");
		System.out.println("start time is" + new Date(startTime) + " end time is " + new Date(endTime));
		// 1497580000000 as it's definitely in the past at the time I wrote this and its
		// a nice even number.

		System.currentTimeMillis();
		// 00ELEGYYMMDDHHSS

		// String bin = "00ELEG";
		String binPlusTimeInMs = bin + Integer.toString((int) System.currentTimeMillis(), 36);
		String CC = binPlusTimeInMs + FourDig; // = binPlusTimeInMs + "0001";
		// System.out.println(binPlusTimeInMs); //00ELEGngf3nx
		return CC;

	}

	private static final String newLine = System.getProperty("line.separator");

	// import org.apache.commons.lang3.StringUtils;

	/*
	 * Pad the string until it is the given length
	 */
	public static String completeWithWhiteSpaces(String pString, int lenght) {

		lenght = lenght - pString.length();

		for (int i = 0; i < lenght; i++)
			pString += " ";
		return pString;
	}

	// @SuppressWarnings("deprecation")
	public static JsonObject getAccountApi(String card) {
		try {
			System.out.println("The card number is ******: " + card);
			// System.out.println("getAccountApi");

			String AugeoId = GetRedirectNewUrl("http://support.cit.augeofi.net/account/?id=" + card);

			String content = AccountAPI(AugeoId);
			System.out.println("The augeoid is: *** " + AugeoId);
			// Now we have augeoID, we want to use an api to look up character info

			URL url = new URL("https://fi-platform.cit.augeofi.net/v1/users/" + AugeoId + "/accountinfo");

			JsonObject items = Json.parse(content).asObject();
			return items;

		} catch (Exception e) {
			System.out.println("ERROR in getAccountApi: ");
			e.printStackTrace();
		}
		return null;
	}

	// Non card Code

	public static String PostToCobtestUrl(URL url, Map<String, Object> params) {
		try {

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String, Object> param : params.entrySet()) {
				if (postData.length() != 0)
					postData.append('&');
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);

			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

			StringBuilder sb = new StringBuilder();
			for (int c; (c = in.read()) >= 0;)
				sb.append((char) c);
			String augeoidpart = sb.toString();
			String augeoid = augeoidpart.substring(24, 35);
			System.out.println("The augeo id is: " + augeoid);
			getAccountApiUsingAugeoId(augeoid);
			return augeoid;

		} catch (IOException e) {

			return "";
		}

	}

	public static JsonObject getAccountApiUsingAugeoId(String AugeoId) {
		try {
			String content = AccountAPI(AugeoId);
			System.out.println("The augeoid is: *** " + AugeoId);
			// Now we have augeoID, we want to use an api to look up character info

			URL url = new URL("https://fi-platform.cit.augeofi.net/v1/users/" + AugeoId + "/accountinfo");

			JsonObject items = Json.parse(content).asObject();
			return items;

		} catch (Exception e) {
			System.out.println("ERROR in getAccountApi: ");
			e.printStackTrace();
		}
		return null;
	}

	
	
	//genpurge
	
	public static JsonObject getAccountApiGenpurge(String card) {
		try {
			System.out.println("The card number is ******: " + card);
			// System.out.println("getAccountApi");

			String AugeoId = GetRedirectNewUrl("http://support.cit.augeofi.net/account/?id=" + card);

			String content = AccountAPI(AugeoId);
			System.out.println("The augeoid is: *** " + AugeoId);
			// Now we have augeoID, we want to use an api to look up character info

			URL url = new URL("https://fi-platform.cit.augeofi.net/v1/users/" + AugeoId + "/accountinfo");

			JsonObject items = Json.parse(content).asObject();
			return items;

		} catch (Exception e) {
			System.out.println("ERROR in getAccountApi: ");
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static synchronized Map<String, String> TableDictionaryConverter(List<List<String>> data) {
		Map<String, String> mapTable = new HashMap<String, String>();
		for (List<String> rows : data) {
			mapTable.put(rows.get(0), rows.get(1));
		}
		return mapTable;
	}

	public static String AccountAPI(String augeoID) {
		try {
			// URL url = new URL("http://support.qa.augeofi.net/account/?id="+augeoID);
			URL url = new URL("https://fi-platform.cit.augeofi.net/v1/users/" + augeoID + "/accountinfo");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			String readStream = readStream(con.getInputStream());
			// Give output for the command line
			// System.out.println(readStream);
			return readStream;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ERROR";

	}

	public static String DPSearchAPI(String augeoID) {
		try {
			// URL url = new URL("http://support.qa.augeofi.net/account/?id="+augeoID);
			URL url = new URL("https://fi-platform.cit.augeofi.net/v1/users/" + augeoID + "/accountinfo");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			String readStream = readStream(con.getInputStream());
			// Give output for the command line
			// System.out.println(readStream);
			return readStream;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ERROR";

	}

	public static String AccountInspector(String augeoID) {
		try {
			URL url = new URL("http://support.cit.augeofi.net/account/?id=" + augeoID);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			String readStream = readStream(con.getInputStream());
			// Give output for the command line
			System.out.println(readStream);
			return readStream;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ERROR";

	}

	public static String readStream(InputStream in) {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in));) {
			String nextLine = "";
			while ((nextLine = reader.readLine()) != null) {
				sb.append(nextLine + newLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();

	}

	public static String uniqueCardOrig() {
		// here m will be in seconds
		long m = System.currentTimeMillis() / 1000;
		long test = 1497580000000L; // definitly in the past
		long reducedInt;

		reducedInt = Math.abs(System.currentTimeMillis() - test);

		// System.out.println("1234567890123456");
		// System.out.println("00ELEGYYMMDDHHSS");
		// System.out.println("System.currentTimeMillis(): "
		// +System.currentTimeMillis());
		// System.out.println("System.currentTimeMillis() m: " + m );
		// System.out.println("System.currentTimeMillis() m - 1497580000000L: " + (m -
		// test));

		// System.out.println("currentTimeMillis reduced: " + (reducedInt));
		// System.out.println("currentTimeMillis reduced: " + (reducedInt));
		// System.out.println("Mil time base 36 : " + Integer.toString((int)
		// (reducedInt), 36) );
		System.out.println("reduced int is " + reducedInt);

		String returnVal = Long.toString(reducedInt, 36);

		returnVal = returnVal.replaceAll("-", "");

		return returnVal;

		// long now = System.currentTimeMillis(); // Simpler way to get current time

		// try {
		// Date date = new SimpleDateFormat("mm dd yyyy h:mm:ss a").parse("07 15 2020
		// 1:00:00 PM");
		// long timeElapsed = now - date.getTime(); // Here's your number of ms
		// System.out.println("Mil time base 36 : " + Integer.toString((int)
		// (timeElapsed), 36) + " timeElapsed" + timeElapsed );
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// Long startTime= Long.parseLong("1497674732168");
		// Long endTime= Long.parseLong("1497574732168");
		// System.out.println("start time is"+new Date(startTime) + " end time is " +
		// new Date(endTime));
		// String binPlusTimeInMs = bin + Integer.toString((int)
		// System.currentTimeMillis(), 36);
		// String CC = binPlusTimeInMs + FourDig; //= binPlusTimeInMs + "0001";
		// return CC;

	}

	public static String GetRedirectNewUrl(String url) {
		try {
			// URL urlA = new URL(url);
			// urlA.openConnection();
			// System.out.println("redirect: " + urlA.getQuery());
			// boolean redirect = false;
			// System.out.println("GetRedirect url: "+url );
			URL u = new URL(url);
			HttpURLConnection.setFollowRedirects(true);
			HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
			httpURLConnection.setInstanceFollowRedirects(true);
			HttpURLConnection.setFollowRedirects(true);
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setReadTimeout(15000);
			httpURLConnection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Firefox/3.0.13");
			System.getProperties().setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1,SSLv3");
			// NORMALLY, the new url is in the "location
			// System.out.println("Location" + newUrl);
			// However for Account inpector here, we just have to look in the getURL
			// Somehow necessary to engage the redirect mechanism to get the augeo id
			String newUrl = httpURLConnection.getHeaderField("Location");
			httpURLConnection.connect();
			httpURLConnection.getContent();
			// USEFUL DEBUG:
			// System.out.println("GetRedirect newUrl: " + httpURLConnection.getURL() );
			String strAugeoId = httpURLConnection.getURL().toString()
					.replace("http://support.cit.augeofi.net/account/?id=", "");
			// USEFUL DEBUG:
			// System.out.println("AUGEOID: " + strAugeoId);
			return strAugeoId;

		} catch (IOException e) {
			// USEFUL DEBUG:
			// System.out.println("IOException newUrl: " );
			// e.printStackTrace();
			return "";
		}

	}

	public static String getAccountApiLegacy(String card) {
		try {

			System.out.println("getAccountApi");
			// String redirect =
			// GetRedirect("http://support.qa.augeofi.net/account/?id="+card);
			// URL url = new URL("http://support.qa.augeofi.net/account/?id="+card);
			// url.openConnection();
			// System.out.println("redirect: " + url.getQuery());
			// System.out.println("augeoid: " + redirect.substring(40));
			//
			// Document doc =
			// Jsoup.connect("\"http://support.qa.augeofi.net/account/?id=\"+card").get();
			// Document doc =
			// Jsoup.connect("http://support.qa.augeofi.net/account/?id="+card).get();
			// System.out.println(doc.text());
			// log(doc.title());
			// Elements newsHeadlines = doc.select("#mp-itn b a");
			/*
			 * Elements Status = doc.select("tr[data-alpha=status]"); for (Element headline
			 * : Status) { // log System.out.println(headline.toString());
			 * System.out.println("%s\n\t%s"+ headline.attr("title")+
			 * headline.absUrl("href")); } Elements Status2 =
			 * doc.select("[data-alpha=status]"); for (Element headline : Status) { // log
			 * System.out.println(headline.toString()); System.out.println("%s\n\t%s"+
			 * headline.attr("title")+ headline.absUrl("href")); } Elements Status2 =
			 * doc.select("[data-alpha=status]"); for (Element headline : Status) { // log
			 * 
			 * System.out.println(headline.toString()); System.out.println("%s\n\t%s"+
			 * headline.attr("title")+ headline.absUrl("href")); }
			 * 
			 * 
			 * 
			 * HttpURLConnection con = (HttpURLConnection) url.openConnection(); String
			 * readStream = readStream(con.getInputStream()); // Give output for the command
			 * line
			 * 
			 * readStream.subSequence(readStream.indexOf("data-alpha=\"status\""), endIndex)
			 * System.out.println(readStream); return readStream;
			 * 
			 */
		} catch (Exception e) {
			System.out.println("ERROR in SoupAccountInspector: ");
			e.printStackTrace();
		}
		return "ERROR";

	}

}
